(function () {
  'use strict';

  angular
    .module('reports')
    .controller('ReportsListController', ReportsListController);

  ReportsListController.$inject = ['ReportsService', 'BudgetsService', 'MonthsService'];

  function ReportsListController(ReportsService, BudgetsService, MonthsService) {
    var vm = this;

    vm.exportData = function () {
      const selectedIndex = document.querySelector("#select").selectedIndex;

      alert('query ' + vm.months[selectedIndex]);
    };

    vm.init = function () {
      const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
      vm.months = months;
    };


    vm.init();
  }
}());
