// Searches service used to communicate Searches REST endpoints
(function () {
  'use strict';

  angular
    .module('searches')
    .factory('SearchesService', SearchesService);

  SearchesService.$inject = ['$http', '$q'];

  function SearchesService($http, $q) {

    function getAllPayments() {
      var deferred = $q.defer();
      var url = '/api/payments/list';

      $http({
        method: 'GET',
        url: url
      }).then(function successCallback(response) {
        deferred.resolve(response);
      }, function errorCallback(response) {
        deferred.reject(response);
      });

      return deferred.promise;
    }

    function getCategoryById(id) {
      var deferred = $q.defer();
      var url = `/api/category/data/${id}`;

      $http({
        method: 'GET',
        url: url
      }).then(function successCallback(response) {
        deferred.resolve(response);
      }, function errorCallback(response) {
        deferred.reject(response);
      });

      return deferred.promise;
    }

    return {
      getPayments: getAllPayments,
      getCategoryById: getCategoryById
    };
  }


}());
