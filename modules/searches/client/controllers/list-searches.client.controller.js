(function () {
  'use strict';

  angular
    .module('searches')
    .controller('SearchesListController', SearchesListController);

  SearchesListController.$inject = ['SearchesService', 'CategoryService'];

  function SearchesListController(SearchesService, CategoryService) {
    var vm = this;

    vm.filterPayments = function (event) {
      let filtered = vm.initialPayments.filter((payment) => {
        // this is a pure hack, but no idea how to take ref in agular
        const inputValue = document.getElementsByClassName("input-field")[0].value;
        return payment.name.includes(inputValue) || payment.category.includes(inputValue) || payment.created.includes(inputValue);
      });

      vm.payments = filtered;
    };

    vm.init = function () {

      const date = new Date();
      const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
      const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];


      SearchesService.getPayments().then(function (result) {
        vm.initialPayments = result.data.map(payment => {
          payment.created = months[new Date(payment.created).getMonth()];
          return payment;
        });

        vm.initialPayments.forEach(payment => {
          SearchesService.getCategoryById(payment.category_id).then(result => {
            payment.category = result.data[0].name;
          });
        });

        vm.payments = [...vm.initialPayments];
      });

    };


    vm.init();
  }
}());
