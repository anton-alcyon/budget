'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
    Incomes = mongoose.model('Incomes'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Budget
 */
exports.create = function(req, res) {
  var income = new Incomes(req.body);
    income.user = req.user;

    income.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(income);
    }
  });
};


exports.list = function (req, res) {

    Incomes.find({ budget_id: req.params.budgetId}).sort('-created').populate('user', 'displayName').exec(function (err, budgets) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(budgets);
        }
    });
};