'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  fs = require('fs'),
  Payments = mongoose.model('Payments'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Budget
 */
exports.create = function (req, res) {
  var payment = new Payments(req.body);
  payment.user = req.user;

  payment.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(payment);
    }
  });
};

exports.list = function (req, res) {

  Payments.find({ category_id: req.params.categoryId }).sort('-created').populate('user', 'displayName').exec(function (err, budgets) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(budgets);
    }
  });
};

exports.listAll = function (req, res) {

  Payments.find({ "user": req.user._id }).sort('-created').populate('user', 'displayName').exec(function (err, payments) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(payments);
    }
  });
};

exports.csv = function (req, res) {
  const month = parseInt(req.params.monthIndex);
  const payment = Payments.find({ "user": req.user._id }).find({ "$where": `this.created.getMonth() === ${month}` }).populate('user', 'displayName')
  
  payment.csv(res);

  // ?!?!?

  // let filename = 'tempReport.csv';
  // let absPath = path.join(__dirname, '/temp/', filename);
  // let relPath = path.join('./temp', filename); // path relative to server root

  // fs.writeFile(relPath, 'File content', (err) => {
  //   if (err) {
  //     console.log(err);
  //   }
  //   res.download(absPath, (err) => {
  //     if (err) {
  //       console.log(err);
  //     }
  //     fs.unlink(relPath, (err) => {
  //       if (err) {
  //         console.log(err);
  //       }
  //       console.log('FILE [' + filename + '] REMOVED!');
  //     });
  //   });
  // });
};
