'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
    mongoose = require('mongoose'),
    Month = mongoose.model('Month'),
    errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
    _ = require('lodash');

/**
 * Create a Budget
 */
exports.create = function (req, res) {
    var month = new Month(req.body);
    month.user = req.user;

    month.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(month);
        }
    });
};

// /**
//  * Show the current Budget
//  */
// exports.read = function (req, res) {
//     // convert mongoose document to JSON
//     var budget = req.budget ? req.budget.toJSON() : {};
//
//     // Add a custom field to the Article, for determining if the current User is the "owner".
//     // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
//     budget.isCurrentUserOwner = req.user && budget.user && budget.user._id.toString() === req.user._id.toString();
//
//     res.jsonp(budget);
// };
//
// /**
//  * Update a Budget
//  */
// exports.update = function (req, res) {
//     var budget = req.budget;
//
//     budget = _.extend(budget, req.body);
//
//     budget.save(function (err) {
//         if (err) {
//             return res.status(400).send({
//                 message: errorHandler.getErrorMessage(err)
//             });
//         } else {
//             res.jsonp(budget);
//         }
//     });
// };
//
// /**
//  * Delete an Budget
//  */
// exports.delete = function (req, res) {
//     var budget = req.budget;
//
//     budget.remove(function (err) {
//         if (err) {
//             return res.status(400).send({
//                 message: errorHandler.getErrorMessage(err)
//             });
//         } else {
//             res.jsonp(budget);
//         }
//     });
// };
//
// /**
//  * List of Budgets
//  */
exports.list = function (req, res) {

    Month.find({"user": req.user._id}).sort('-created').populate('user', 'displayName').exec(function (err, budgets) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(budgets);
        }
    });
};
//
// /**
//  * Budget middleware
//  */
// exports.budgetByID = function (req, res, next, id) {
//
//     if (!mongoose.Types.ObjectId.isValid(id)) {
//         return res.status(400).send({
//             message: 'Budget is invalid'
//         });
//     }
//
//     Budget.findById(id).populate('user', 'displayName').exec(function (err, budget) {
//         if (err) {
//             return next(err);
//         } else if (!budget) {
//             return res.status(404).send({
//                 message: 'No Budget with that identifier has been found'
//             });
//         }
//         req.budget = budget;
//         next();
//     });
// };
