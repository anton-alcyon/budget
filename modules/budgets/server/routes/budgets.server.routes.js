'use strict';

/**
 * Module dependencies
 */
var budgets = require('../controllers/budgets.server.controller'),
    category = require('../controllers/category.server.controller'),
    payments = require('../controllers/payments.server.controller'),
    incomes = require('../controllers/incomes.server.controller'),
    months = require('../controllers/months.server.controller');

module.exports = function (app) {
    // Budgets Routes
    app.route('/api/budgets')
        .post(budgets.create);

    app.route('/api/budgets/:monthId')
        .get(budgets.list);

    app.route('/api/budgets/:budgetId')
        .get(budgets.read)
        .put(budgets.update)
        .delete(budgets.delete);

    app.route('/api/month')
        .post(months.create)
        .get(months.list);

    app.route('/api/category')
        .post(category.create);

    app.route('/api/category/:budgetId')
        .get(category.list);

    app.route('/api/category/data/:categoryId')
        .get(category.getCategory);

    app.route('/api/payments')
        .post(payments.create);

    app.route('/api/payments/list')
        .get(payments.listAll);

    app.route('/api/payments/:categoryId')
        .get(payments.list);

    app.route('/api/download/:monthIndex')
        .get(payments.csv);

    app.route('/api/incomes')
        .post(incomes.create);

    app.route('/api/incomes/:budgetId')
        .get(incomes.list);
};
