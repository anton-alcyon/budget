'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    mongoose_csv = require('mongoose-csv');

/**
 * Schema
 */
var PaymentsSchema = new Schema({
    name: {
        type: String,
        default: '',
        trim: true
    },
    amount: {
        type: Number,
        default: 0
    },
    category_id: {
        type: String,
        default: '',
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

PaymentsSchema.plugin(mongoose_csv);

mongoose.model('Payments', PaymentsSchema);
