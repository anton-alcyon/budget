'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Schema
 */
var IncomesSchema = new Schema({
    name: {
        type: String,
        default: '',
        trim: true
    },
    amount: {
        type: Number,
        default: 0
    },
    budget_id: {
        type: String,
        default: '',
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

mongoose.model('Incomes', IncomesSchema);
