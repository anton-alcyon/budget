'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Budget Schema
 */
var BudgetSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill Budget name',
        trim: true
    },
    shared: {
        type: String,
        default: '',
        trim: true
    },
    month_id: {
        type: String,
        default: '',
        required: 'Please fill Budget name',
        trim: true
    },
    month_order: {
        type: Number,
        default: 0
    },
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

mongoose.model('Budget', BudgetSchema);
