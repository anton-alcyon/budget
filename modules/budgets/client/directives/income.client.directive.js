(function () {
    'use strict';

    angular
        .module('budgets')
        .directive('incomeList', function () {
            return {
                restrict: 'E',
                // replace: true,
                templateUrl: '/modules/budgets/client/directives/views/list-income.client.view.html',
                scope: {
                    budget: '=',
                    sum: '='
                },
                controller: incomeList
            };
        });

    incomeList.$inject = ['$scope', 'IncomesService', '$uibModal', 'Notification'];

    function incomeList($scope, IncomesService, $uibModal, Notification) {

        $scope.addIncome = function (budget) {

            var modalIncomeInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'income.modal.client.view.html',
                controller: 'IncomeModalClientCtrl',
                controllerAs: '$ctrl',
                resolve: {
                    budget: function () {
                        return budget.name;
                    }
                }
            });

            modalIncomeInstance.result.then(function (data) {
                IncomesService.addIncome({
                    'income': data.income,
                    'budget': budget,
                    'amount': data.amount
                }).then(function () {
                    Notification.success({message: '<i class="glyphicon glyphicon-ok"></i> Add income successful!'});
                    $scope.init();
                });
            });
        };

        $scope.calculate = function (income, records) {
            $scope.sum = 0;
            angular.forEach(records, function (record) {
                $scope.sum += record.amount;
            })
        };

        $scope.init = function () {
            IncomesService.getIncomes($scope.budget._id).then(function (result) {
                $scope.incomes = result.data;

                $scope.calculate($scope.sum, $scope.incomes);
            })
        }

        $scope.init();
    }

})();

angular.module('budgets').controller('IncomeModalClientCtrl', function ($uibModalInstance, budget) {

    var $ctrl = this;
    $ctrl.name = budget;

    $ctrl.ok = function () {
        $uibModalInstance.close({'income': $ctrl.income, 'amount': $ctrl.amount});
    };

    $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});