(function () {
    'use strict';

    angular
        .module('budgets')
        .directive('paymentsList', function () {
            return {
                restrict: 'A',
                // replace: true,
                templateUrl: '/modules/budgets/client/directives/views/list-payments.client.view.html',
                scope: {
                    category: '=',
                    sum: '=',
                    budget: '='
                },
                controller: paymentsList
            };
        });

    paymentsList.$inject = ['$scope', 'PaymentsService'];

    function paymentsList($scope, PaymentsService) {

        $scope.calculate = function (income, records) {
            $scope.sum = 0;

            angular.forEach(records, function (record) {
                $scope.sum += record.amount;
            })

        };

        PaymentsService.getPayments($scope.category._id).then(function (result) {
            $scope.paymets = result.data;

            $scope.calculate($scope.sum, $scope.paymets);
        });
    }

})();
