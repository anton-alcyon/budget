(function () {
    'use strict';

    angular
        .module('budgets')
        .directive('categoryList', function () {
            return {
                restrict: 'E',
                templateUrl: '/modules/budgets/client/directives/views/list-categories.client.view.html',
                scope: {
                    budget: '=',
                    calculate: '='
                },
                controller: categoryList
            };
        });

    categoryList.$inject = ['$scope', 'CategoryService', '$uibModal', 'PaymentsService', 'Notification', '$state'];

    function categoryList($scope, CategoryService, $uibModal, PaymentsService, Notification, $state) {

        $scope.sum = 0;

        $scope.budget.reloadCategories = function () {
            $scope.init();
        };

        $scope.addPayment = function (category, budget) {

            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'payment.modal.client.view.html',
                controller: 'PaymentModalClientCtrl',
                controllerAs: '$ctrl',
                resolve: {
                    category: function () {
                        return category.name;
                    },
                    budget: function () {
                        return $scope.budget.name;
                    }
                }
            });

            modalInstance.result.then(function (payment) {

                payment.category = category;

                PaymentsService.addPayment(payment).then(function () {
                    Notification.success({message: '<i class="glyphicon glyphicon-ok"></i> Add payment successful!'});
                    $state.go($state.current, {}, {reload: true});
                });
            });
        }

        $scope.init = function () {
            CategoryService.getCategories($scope.budget._id).then(function (result) {

                $scope.budget.sum = [];

                $scope.categories = result.data;

                $scope.$watch('categories', function (newValue, oldValue) {
                    $scope.budget.sum = [];
                    angular.forEach(newValue, function (record) {
                        $scope.budget.sum.push(record);
                    })
                });
            });
        };

        $scope.init();
    }

})();

angular.module('budgets').controller('PaymentModalClientCtrl', function ($uibModalInstance, category, budget) {

    var $ctrl = this;

    $ctrl.category = category;
    $ctrl.budget = budget;

    $ctrl.ok = function () {
        $uibModalInstance.close({'payment': $ctrl.payment, 'amount': $ctrl.amount});
    };

    $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});