'use strict';

angular
    .module('budgets')
    .factory('BudgetsService', BudgetsService);

BudgetsService.$inject = ['$http', '$q'];

function BudgetsService($http, $q) {

    function addBudget(name, monthId, shared, monthOrder) {

        var deferred = $q.defer();
        var url = '/api/budgets';

        $http({
            method: 'POST',
            url: url,
            data: {
                'name': name,
                'month_id': monthId,
                'shared': shared,
                'month_order': monthOrder
            }
        }).then(function successCallback(response) {
            deferred.resolve(response);
        }, function errorCallback(response) {
            deferred.reject(response);
        });

        return deferred.promise;
    }

    function getBudgets(monthId) {

        var deferred = $q.defer();
        var url = '/api/budgets/' + monthId;

        $http({
            method: 'GET',
            url: url
        }).then(function successCallback(response) {
            deferred.resolve(response);
        }, function errorCallback(response) {
            deferred.reject(response);
        });

        return deferred.promise;
    }

    return {
        addBudget: addBudget,
        getBudgets: getBudgets
    };
}

