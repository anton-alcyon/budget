'use strict';

angular
    .module('budgets')
    .factory('CategoryService', CategoryService);

CategoryService.$inject = ['$http', '$q'];

function CategoryService($http, $q) {

    function addCategory(name, budget_id, budget) {

        var deferred = $q.defer();
        var url = '/api/category';

        $http({
            method: 'POST',
            url: url,
            data: {
                'name': name,
                'budget_id': budget_id,
                'budget': budget
            }
        }).then(function successCallback(response) {
            deferred.resolve(response);
        }, function errorCallback(response) {
            deferred.reject(response);
        });

        return deferred.promise;
    }

    function getCategories(budgetId) {

        var deferred = $q.defer();
        var url = '/api/category/' + budgetId;

        $http({
            method: 'GET',
            url: url
        }).then(function successCallback(response) {
            deferred.resolve(response);
        }, function errorCallback(response) {
            deferred.reject(response);
        });

        return deferred.promise;
    }

    return {
        addCategory: addCategory,
        getCategories: getCategories
    };
}

