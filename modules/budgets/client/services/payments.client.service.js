'use strict';

angular
    .module('budgets')
    .factory('PaymentsService', PaymentsService);

PaymentsService.$inject = ['$http', '$q'];

function PaymentsService($http, $q) {

    function addPayment(data) {
        const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        const selectedMonth = document.querySelector('[ng-model="vm.selectedMonth"].active').innerText;
        const monthIndex = months.indexOf(selectedMonth);

        const date = new Date(2018, monthIndex, 1);

        debugger;

        var deferred = $q.defer();
        var url = '/api/payments';

        $http({
            method: 'POST',
            url: url,
            data: {
                'name': data.payment,
                'amount': data.amount,
                'category_id': data.category._id,
                'created': date
            }
        }).then(function successCallback(response) {
            deferred.resolve(response);
        }, function errorCallback(response) {
            deferred.reject(response);
        });

        return deferred.promise;
    }

    function getPayments(categoryId) {

        var deferred = $q.defer();
        var url = '/api/payments/' + categoryId;

        $http({
            method: 'GET',
            url: url
        }).then(function successCallback(response) {
            deferred.resolve(response);
        }, function errorCallback(response) {
            deferred.reject(response);
        });

        return deferred.promise;
    }

    return {
        addPayment: addPayment,
        getPayments: getPayments
    };
}

