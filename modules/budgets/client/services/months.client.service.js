'use strict';

angular
    .module('budgets')
    .factory('MonthsService', MonthsService);

MonthsService.$inject = ['$http', '$q'];

function MonthsService($http, $q) {

    function addMonth(id) {

        var deferred = $q.defer();
        var url = '/api/month';

        $http({
            method: 'POST',
            url: url,
            data: {
                'id': id
            }
        }).then(function successCallback(response) {
            deferred.resolve(response);
        }, function errorCallback(response) {
            deferred.reject(response);
        });

        return deferred.promise;
    }

    function getMonth() {

        var deferred = $q.defer();
        var url = '/api/month';

        $http({
            method: 'GET',
            url: url
        }).then(function successCallback(response) {
            deferred.resolve(response);
        }, function errorCallback(response) {
            deferred.reject(response);
        });

        return deferred.promise;
    }

    return {
        addMonth: addMonth,
        getMonth: getMonth
    };
}

