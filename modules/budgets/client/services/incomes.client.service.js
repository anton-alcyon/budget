'use strict';

angular
    .module('budgets')
    .factory('IncomesService', IncomesService);

IncomesService.$inject = ['$http', '$q'];

function IncomesService($http, $q) {

    function addIncome(data) {

        var deferred = $q.defer();
        var url = '/api/incomes';

        $http({
            method: 'POST',
            url: url,
            data: {
                'name': data.income,
                'amount': data.amount,
                'budget_id': data.budget._id
            }
        }).then(function successCallback(response) {
            deferred.resolve(response);
        }, function errorCallback(response) {
            deferred.reject(response);
        });

        return deferred.promise;
    }

    function getIncomes(budgetId) {

        var deferred = $q.defer();
        var url = '/api/incomes/' + budgetId;

        $http({
            method: 'GET',
            url: url
        }).then(function successCallback(response) {
            deferred.resolve(response);
        }, function errorCallback(response) {
            deferred.reject(response);
        });

        return deferred.promise;
    }

    return {
        addIncome: addIncome,
        getIncomes: getIncomes
    };
}

