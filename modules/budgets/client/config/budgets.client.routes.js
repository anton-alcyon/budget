(function () {
  'use strict';

  angular
    .module('budgets')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('budgets', {
        abstract: true,
        url: '/budgets',
        template: '<ui-view/>'
      })
      .state('budgets.list', {
        url: '',
        templateUrl: '/modules/budgets/client/views/list-budgets.client.view.html',
        controller: 'BudgetsListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Budgets List'
        }
      })
  }
}());
