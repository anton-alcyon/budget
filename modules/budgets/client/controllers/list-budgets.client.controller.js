(function () {
    'use strict';

    angular
        .module('budgets')
        .controller('BudgetsListController', BudgetsListController);

    BudgetsListController.$inject = ['Authentication', '$state', '$uibModal', 'BudgetsService', 'Notification', 'CategoryService', 'MonthsService', '$scope', '$timeout'];

    function BudgetsListController(Authentication, $state, $uibModal, BudgetsService, Notification, CategoryService, MonthsService, $scope, $timeout) {

        var vm = this;
        vm.budgets = [];

        vm.list = [
            {
                name: 'January',
                id: 0
            },
            {
                name: 'February',
                id: 1
            },
            {
                name: 'March',
                id: 2
            },
            {
                name: 'April',
                id: 3
            },
            {
                name: 'May',
                id: 4
            },
            {
                name: 'June',
                id: 5
            },
            {
                name: 'July',
                id: 6
            },
            {
                name: 'August',
                id: 7
            },
            {
                name: 'September',
                id: 8
            },
            {
                name: 'October',
                id: 9
            },
            {
                name: 'November',
                id: 10
            },
            {
                name: 'December',
                id: 11
            }
        ];

        if (!Authentication.user) {
            $state.go('home', {}, {reload: true})
        }

        vm.getMonthName = function (id) {

            var exist = vm.list.filter(function (record) {
                return record.id == id
            });

            return exist[0].name;
        }

        vm.openMonthModal = function () {

            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'month.modal.client.view.html',
                controller: 'MonthModalClientCtrl',
                controllerAs: '$ctrl',
                resolve: {
                    months: function () {
                        return vm.months;
                    },
                    list: function () {
                        return vm.list;
                    }
                }
            });

            modalInstance.result.then(function (selected) {
                MonthsService.addMonth(selected.id).then(function () {
                    Notification.success({message: '<i class="glyphicon glyphicon-ok"></i> Add month successful!'});
                    vm.init();
                });
            });
        };

        vm.openBudgetModal = function (monthId) {

            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'budget.modal.client.view.html',
                controller: 'BudgetModalClientCtrl',
                controllerAs: '$ctrl',
                resolve: {
                    month: function () {
                        return vm.getMonthName(monthId);
                    }
                }
            });

            var recordId = vm.months.filter(function (record) {
                return record.id == monthId;
            });

            modalInstance.result.then(function (record) {
                BudgetsService.addBudget(record.name, recordId[0]._id, record.shared, recordId[0].id).then(function () {
                    Notification.success({message: '<i class="glyphicon glyphicon-ok"></i> Add budget successful!'});

                    BudgetsService.getBudgets(recordId[0]._id).then(function (result) {
                        vm.budgets = result.data;
                    });
                });
            });
        };

        vm.openCategoryModal = function (budget) {

            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'category.modal.client.view.html',
                controller: 'CategoryModalClientCtrl',
                controllerAs: '$ctrl',
                resolve: {
                    budget: function () {
                        return budget.name;
                    }
                }
            });

            modalInstance.result.then(function (result) {
                CategoryService.addCategory(result.category, budget._id, result.budget).then(function () {

                    budget.reloadCategories();

                    Notification.success({message: '<i class="glyphicon glyphicon-ok"></i> Add category successful!'});
                });
            });
        };

        $scope.$watch('vm.budgets', function () {

            angular.forEach(vm.budgets, function (budget) {
                budget.all = 0;
                $timeout(function () {
                    angular.forEach(budget.sum, function (b) {
                        console.log(angular.copy(b));
                        budget.all += b.sum
                    })

                    budget.showCalculation = true;
                }, 1000);
            });
        });

        $scope.$watch('vm.selectedMonth', function (newValue, oldValue) {
            if (newValue !== oldValue) {

                var recordId = vm.months.filter(function (record) {
                    return record.id == newValue;
                });

                BudgetsService.getBudgets(recordId[0]._id).then(function (result) {
                    vm.budgets = result.data;
                });
            }
        });

        vm.init = function () {

            MonthsService.getMonth().then(function (result) {
                vm.months = result.data;

                if (vm.months.length) {
                    vm.selectedMonth = vm.months[0].id;
                }
            });
        };

        vm.init();
    }
}());

angular.module('budgets').controller('BudgetModalClientCtrl', function ($uibModalInstance, month) {

    var $ctrl = this;

    $ctrl.month = month;

    $ctrl.ok = function () {
        $uibModalInstance.close({"name": $ctrl.name, "shared": $ctrl.shared});
    };

    $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

angular.module('budgets').controller('CategoryModalClientCtrl', function ($uibModalInstance, budget) {

    var $ctrl = this;
    $ctrl.name = budget;

    $ctrl.ok = function () {
        $uibModalInstance.close({"category": $ctrl.category, "budget": $ctrl.budget});
    };

    $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

angular.module('budgets').controller('MonthModalClientCtrl', function ($uibModalInstance, months, list) {

    var $ctrl = this;

    $ctrl.months = months;

    $ctrl.alreadySelected = function (month) {

        var exist = $ctrl.months.filter(function (record) {
            return record.id == month.id
        });

        return exist.length ? true : false
    };

    $ctrl.month = list;

    $ctrl.ok = function (month) {
        $uibModalInstance.close(month);
    };

    $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});