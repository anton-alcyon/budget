'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Budget = mongoose.model('Budget'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  budget;

/**
 * Budget routes tests
 */
describe('Budget CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Budget
    user.save(function () {
      budget = {
        name: 'Budget name'
      };

      done();
    });
  });

  it('should be able to save a Budget if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Budget
        agent.post('/api/budgets')
          .send(budget)
          .expect(200)
          .end(function (budgetSaveErr, budgetSaveRes) {
            // Handle Budget save error
            if (budgetSaveErr) {
              return done(budgetSaveErr);
            }

            // Get a list of Budgets
            agent.get('/api/budgets')
              .end(function (budgetsGetErr, budgetsGetRes) {
                // Handle Budgets save error
                if (budgetsGetErr) {
                  return done(budgetsGetErr);
                }

                // Get Budgets list
                var budgets = budgetsGetRes.body;

                // Set assertions
                (budgets[0].user._id).should.equal(userId);
                (budgets[0].name).should.match('Budget name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Budget if not logged in', function (done) {
    agent.post('/api/budgets')
      .send(budget)
      .expect(403)
      .end(function (budgetSaveErr, budgetSaveRes) {
        // Call the assertion callback
        done(budgetSaveErr);
      });
  });

  it('should not be able to save an Budget if no name is provided', function (done) {
    // Invalidate name field
    budget.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Budget
        agent.post('/api/budgets')
          .send(budget)
          .expect(400)
          .end(function (budgetSaveErr, budgetSaveRes) {
            // Set message assertion
            (budgetSaveRes.body.message).should.match('Please fill Budget name');

            // Handle Budget save error
            done(budgetSaveErr);
          });
      });
  });

  it('should be able to update an Budget if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Budget
        agent.post('/api/budgets')
          .send(budget)
          .expect(200)
          .end(function (budgetSaveErr, budgetSaveRes) {
            // Handle Budget save error
            if (budgetSaveErr) {
              return done(budgetSaveErr);
            }

            // Update Budget name
            budget.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Budget
            agent.put('/api/budgets/' + budgetSaveRes.body._id)
              .send(budget)
              .expect(200)
              .end(function (budgetUpdateErr, budgetUpdateRes) {
                // Handle Budget update error
                if (budgetUpdateErr) {
                  return done(budgetUpdateErr);
                }

                // Set assertions
                (budgetUpdateRes.body._id).should.equal(budgetSaveRes.body._id);
                (budgetUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Budgets if not signed in', function (done) {
    // Create new Budget model instance
    var budgetObj = new Budget(budget);

    // Save the budget
    budgetObj.save(function () {
      // Request Budgets
      request(app).get('/api/budgets')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Budget if not signed in', function (done) {
    // Create new Budget model instance
    var budgetObj = new Budget(budget);

    // Save the Budget
    budgetObj.save(function () {
      request(app).get('/api/budgets/' + budgetObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', budget.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Budget with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/budgets/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Budget is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Budget which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Budget
    request(app).get('/api/budgets/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Budget with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Budget if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Budget
        agent.post('/api/budgets')
          .send(budget)
          .expect(200)
          .end(function (budgetSaveErr, budgetSaveRes) {
            // Handle Budget save error
            if (budgetSaveErr) {
              return done(budgetSaveErr);
            }

            // Delete an existing Budget
            agent.delete('/api/budgets/' + budgetSaveRes.body._id)
              .send(budget)
              .expect(200)
              .end(function (budgetDeleteErr, budgetDeleteRes) {
                // Handle budget error error
                if (budgetDeleteErr) {
                  return done(budgetDeleteErr);
                }

                // Set assertions
                (budgetDeleteRes.body._id).should.equal(budgetSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Budget if not signed in', function (done) {
    // Set Budget user
    budget.user = user;

    // Create new Budget model instance
    var budgetObj = new Budget(budget);

    // Save the Budget
    budgetObj.save(function () {
      // Try deleting Budget
      request(app).delete('/api/budgets/' + budgetObj._id)
        .expect(403)
        .end(function (budgetDeleteErr, budgetDeleteRes) {
          // Set message assertion
          (budgetDeleteRes.body.message).should.match('User is not authorized');

          // Handle Budget error error
          done(budgetDeleteErr);
        });

    });
  });

  it('should be able to get a single Budget that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Budget
          agent.post('/api/budgets')
            .send(budget)
            .expect(200)
            .end(function (budgetSaveErr, budgetSaveRes) {
              // Handle Budget save error
              if (budgetSaveErr) {
                return done(budgetSaveErr);
              }

              // Set assertions on new Budget
              (budgetSaveRes.body.name).should.equal(budget.name);
              should.exist(budgetSaveRes.body.user);
              should.equal(budgetSaveRes.body.user._id, orphanId);

              // force the Budget to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Budget
                    agent.get('/api/budgets/' + budgetSaveRes.body._id)
                      .expect(200)
                      .end(function (budgetInfoErr, budgetInfoRes) {
                        // Handle Budget error
                        if (budgetInfoErr) {
                          return done(budgetInfoErr);
                        }

                        // Set assertions
                        (budgetInfoRes.body._id).should.equal(budgetSaveRes.body._id);
                        (budgetInfoRes.body.name).should.equal(budget.name);
                        should.equal(budgetInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Budget.remove().exec(done);
    });
  });
});
