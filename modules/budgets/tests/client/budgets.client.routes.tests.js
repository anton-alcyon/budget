(function () {
  'use strict';

  describe('Budgets Route Tests', function () {
    // Initialize global variables
    var $scope,
      BudgetsService;

    // We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _BudgetsService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      BudgetsService = _BudgetsService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('budgets');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/budgets');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          BudgetsController,
          mockBudget;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('budgets.view');
          $templateCache.put('modules/budgets/client/views/view-budget.client.view.html', '');

          // create mock Budget
          mockBudget = new BudgetsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Budget Name'
          });

          // Initialize Controller
          BudgetsController = $controller('BudgetsController as vm', {
            $scope: $scope,
            budgetResolve: mockBudget
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:budgetId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.budgetResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            budgetId: 1
          })).toEqual('/budgets/1');
        }));

        it('should attach an Budget to the controller scope', function () {
          expect($scope.vm.budget._id).toBe(mockBudget._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/budgets/client/views/view-budget.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          BudgetsController,
          mockBudget;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('budgets.create');
          $templateCache.put('modules/budgets/client/views/form-budget.client.view.html', '');

          // create mock Budget
          mockBudget = new BudgetsService();

          // Initialize Controller
          BudgetsController = $controller('BudgetsController as vm', {
            $scope: $scope,
            budgetResolve: mockBudget
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.budgetResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/budgets/create');
        }));

        it('should attach an Budget to the controller scope', function () {
          expect($scope.vm.budget._id).toBe(mockBudget._id);
          expect($scope.vm.budget._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/budgets/client/views/form-budget.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          BudgetsController,
          mockBudget;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('budgets.edit');
          $templateCache.put('modules/budgets/client/views/form-budget.client.view.html', '');

          // create mock Budget
          mockBudget = new BudgetsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Budget Name'
          });

          // Initialize Controller
          BudgetsController = $controller('BudgetsController as vm', {
            $scope: $scope,
            budgetResolve: mockBudget
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:budgetId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.budgetResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            budgetId: 1
          })).toEqual('/budgets/1/edit');
        }));

        it('should attach an Budget to the controller scope', function () {
          expect($scope.vm.budget._id).toBe(mockBudget._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/budgets/client/views/form-budget.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
}());
