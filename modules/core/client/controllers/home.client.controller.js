(function () {
    'use strict';

    angular
        .module('core')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['Authentication', '$state'];


    function HomeController(Authentication, $state) {
        var vm = this;

        if (Authentication.user) {
            $state.go('budgets.list', {},{reload: true})
        }

    }
}());
